interface Student {
  name: string;
  homework: number[];
  quizzes: number[];
  tests: number[];
}

const lloyd: Student = {
  name: 'Lloyd',
  homework: [90.0, 97.0, 75.0, 92.0],
  quizzes: [88.0, 40.0, 94.0],
  tests: [75.0, 90.0]
};
const alice: Student = {
  name: 'Alice',
  homework: [100.0, 92.0, 98.0, 100.0],
  quizzes: [82.0, 83.0, 91.0],
  tests: [89.0, 97.0]
};
const tyler: Student = {
  name: 'Tyler',
  homework: [0.0, 87.0, 75.0, 22.0],
  quizzes: [0.0, 75.0, 78.0],
  tests: [100.0, 100.0]
};

let studentList: Student[] = [lloyd, alice, tyler];

function average(scores: number[]): number {
  return (
    scores.reduce((accumulator, currentValue) => accumulator + currentValue, 0) / scores.length
  );
}

function getAverage(student: Student): number {
  let homework = average(student.homework) * 0.1;
  let quizzes = average(student.quizzes) * 0.3;
  let tests = average(student.tests) * 0.6;
  let sum = homework + quizzes + tests;
  return sum;
}

function getLetterGrade(score: number): string {
  if (score >= 90) {
    return 'A';
  } else if (score >= 80) {
    return 'B';
  } else if (score >= 70) {
    return 'C';
  } else if (score >= 60) {
    return 'D';
  } else {
    return 'F';
  }
}

function getClassAverage(classList: Student[]): number {
  let results = [];
  for (let student of classList) {
    results.push(getAverage(student));
  }
  return average(results);
}

console.log(average(lloyd.homework));
console.log(getClassAverage(studentList));
